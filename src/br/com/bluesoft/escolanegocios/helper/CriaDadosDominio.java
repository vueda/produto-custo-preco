package br.com.bluesoft.escolanegocios.helper;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import br.com.bluesoft.escolanegocios.model.COFINS;
import br.com.bluesoft.escolanegocios.model.ICMS;
import br.com.bluesoft.escolanegocios.model.PIS;

public class CriaDadosDominio {

    public static Map<Integer, ICMS> criaICMS(){
        Map<Integer, ICMS> mapICMS = new HashMap<>();
        mapICMS.put(ICMS.ICMS_12_REDUCAO_41_67, new ICMS(ICMS.ICMS_12_REDUCAO_41_67, BigDecimal.valueOf(12), BigDecimal.valueOf(41.67), BigDecimal.valueOf(7),"ICMS 12.00% Redução 41.67%"));
        return mapICMS;
    }

    public static Map<Integer, COFINS> criaCOFINS(){
        Map<Integer, COFINS> mapCOFINS = new HashMap<>();
        mapCOFINS.put(COFINS.TRIBUTADO_7_6, new COFINS(COFINS.TRIBUTADO_7_6, BigDecimal.valueOf(7.6), "Trib 7,6%"));
        return mapCOFINS;
    }

    public static Map<Integer, PIS> criaPIS(){
        Map<Integer, PIS> mapPIS = new HashMap<>();
        mapPIS.put(PIS.TRIBUTADO_1_65, new PIS(PIS.TRIBUTADO_1_65, BigDecimal.valueOf(1.65), "Trib 1,65%"));
        return mapPIS;
    }
}
