package br.com.bluesoft.escolanegocios.model;

import java.math.BigDecimal;
import java.util.Set;

public class Produto {

    public static final BigDecimal CEM = BigDecimal.valueOf(100);

    private BigDecimal custoReposicao;

    private Set<Imposto> impostos;

    private BigDecimal margem;

    private BigDecimal precoVenda;

    public Produto(BigDecimal custoReposicao, Set<Imposto> impostos, BigDecimal margem, BigDecimal precoVenda) {
        this.custoReposicao = custoReposicao;
        this.impostos = impostos;
        this.margem = margem;
        this.precoVenda = precoVenda;
    }

    public BigDecimal getCustoLiquido() {
        double valorImpostos = this.impostos
            .stream()
            .filter(Imposto::isCreditavelEntrada)
            .mapToDouble(imposto -> {
                BigDecimal porcentagemAliquota = imposto.getAliquota().divide(CEM);
                return this.custoReposicao.multiply(porcentagemAliquota).doubleValue();
            })
            .sum();

        return custoReposicao.subtract(BigDecimal.valueOf(valorImpostos));
    }

    public BigDecimal getPrecoMargemZero() {
        double porcentagemImpostosIncidenteNaVenda = this.impostos
            .stream()
            .filter(Imposto::isIncidenteNaVenda)
            .mapToDouble(imposto -> imposto.getAliquota().doubleValue())
            .sum();

        BigDecimal valorPorcentagem = CEM.subtract(BigDecimal.valueOf(porcentagemImpostosIncidenteNaVenda));

        return getCustoLiquido().divide(valorPorcentagem, BigDecimal.ROUND_DOWN).multiply(CEM);
    }

    public BigDecimal getPrecoVendaBruta() {
        double porcentagemImpostosIncidenteNaVenda = this.impostos
            .stream()
            .filter(Imposto::isIncidenteNaVenda)
            .mapToDouble(imposto -> imposto.getAliquota().doubleValue())
            .sum();

        BigDecimal valorPorcentagem = CEM
            .subtract(BigDecimal.valueOf(porcentagemImpostosIncidenteNaVenda))
            .subtract(this.margem);

        return getCustoLiquido().divide(valorPorcentagem, BigDecimal.ROUND_DOWN).multiply(CEM);
    }

    public BigDecimal getMargemVendaBruta() {
        double porcentagemImpostosIncidenteNaVenda = this.impostos
            .stream()
            .filter(Imposto::isIncidenteNaVenda)
            .mapToDouble(imposto -> imposto.getAliquota().doubleValue())
            .sum();

        BigDecimal custoLiquidoPorPrecoVendaBruta = getCustoLiquido().multiply(CEM).divide(precoVenda, BigDecimal.ROUND_DOWN);

        return CEM.subtract(BigDecimal.valueOf(porcentagemImpostosIncidenteNaVenda)).subtract(custoLiquidoPorPrecoVendaBruta);
    }
}
