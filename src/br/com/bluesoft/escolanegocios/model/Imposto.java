package br.com.bluesoft.escolanegocios.model;

import java.math.BigDecimal;

public interface Imposto {

    boolean isCreditavelEntrada();

    boolean isIncidenteNaVenda();

    BigDecimal getAliquota();
}
