package br.com.bluesoft.escolanegocios.model;

import java.math.BigDecimal;

public class PIS implements Imposto {

    public static final Integer TRIBUTADO_1_65 = 1;

    private Integer key;
    private BigDecimal aliquota;
    private String descricao;

    public PIS(Integer key, BigDecimal aliquota, String descricao){
        this.key = key;
        this.descricao = descricao;
        this.aliquota = aliquota;
    }

    @Override
    public boolean isCreditavelEntrada() {
        return true;
    }

    @Override
    public boolean isIncidenteNaVenda() {
        return true;
    }

    @Override
    public BigDecimal getAliquota() {
        return aliquota;
    }
}
