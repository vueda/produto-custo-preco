package br.com.bluesoft.escolanegocios.model;

import java.math.BigDecimal;

public class ICMS implements Imposto{

    public static final Integer ICMS_12_REDUCAO_41_67 = 248;

    private Integer key;
    private BigDecimal aliquota;
    private BigDecimal reducao;
    private BigDecimal carga;
    private String descricao;

    public ICMS(Integer key, BigDecimal aliquota, BigDecimal reducao, BigDecimal carga, String descricao){
        this.key = key;
        this.descricao = descricao;
        this.aliquota = aliquota;
        this.reducao = reducao;
        this.carga = carga;
    }

    @Override
    public boolean isCreditavelEntrada() {
        return true;
    }

    @Override
    public boolean isIncidenteNaVenda() {
        return true;
    }

    @Override
    public BigDecimal getAliquota() { return carga; }
}
