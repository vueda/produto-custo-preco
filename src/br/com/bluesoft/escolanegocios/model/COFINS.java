package br.com.bluesoft.escolanegocios.model;

import java.math.BigDecimal;

public class COFINS implements Imposto {

    public static final Integer TRIBUTADO_7_6 = 1;

    private Integer key;
    private BigDecimal aliquota;
    private String descricao;

    public COFINS(Integer key, BigDecimal aliquota, String descricao){
        this.key = key;
        this.descricao = descricao;
        this.aliquota = aliquota;
    }

    @Override
    public boolean isCreditavelEntrada() {
        return true;
    }

    @Override
    public boolean isIncidenteNaVenda() {
        return true;
    }

    @Override
    public BigDecimal getAliquota() { return aliquota; }
}
