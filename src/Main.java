import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import br.com.bluesoft.escolanegocios.model.COFINS;
import br.com.bluesoft.escolanegocios.model.ICMS;
import br.com.bluesoft.escolanegocios.model.Imposto;
import br.com.bluesoft.escolanegocios.model.PIS;
import br.com.bluesoft.escolanegocios.model.Produto;

import static br.com.bluesoft.escolanegocios.helper.CriaDadosDominio.criaCOFINS;
import static br.com.bluesoft.escolanegocios.helper.CriaDadosDominio.criaICMS;
import static br.com.bluesoft.escolanegocios.helper.CriaDadosDominio.criaPIS;

public class Main {

    public static void main(String[] args) {
        Map<Integer, COFINS> mapaCOFINS = criaCOFINS();
        Map<Integer, ICMS> mapaICMS = criaICMS();
        Map<Integer, PIS> mapaPIS = criaPIS();

        Set<Imposto> impostos = new HashSet<>();
        impostos.add(mapaCOFINS.get(COFINS.TRIBUTADO_7_6));
        impostos.add(mapaPIS.get(PIS.TRIBUTADO_1_65));
        impostos.add(mapaICMS.get(ICMS.ICMS_12_REDUCAO_41_67));

        Produto produto = new Produto(BigDecimal.valueOf(5.0), impostos, BigDecimal.valueOf(22), BigDecimal.valueOf(10));

        System.out.println("CUSTO LIQUIDO ::: " + produto.getCustoLiquido());
        System.out.println("PRECO MARGEM ZERO (PMZ) ::: " + produto.getPrecoMargemZero());
        System.out.println("PRECO VENDA BRUTA ::: " + produto.getPrecoVendaBruta());
        System.out.println("MARGEM VENDA BRUTA ::: " + produto.getMargemVendaBruta());
    }
}
